﻿namespace TreeFromTable
{
    partial class TreeFromTable
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tvData = new System.Windows.Forms.TreeView();
            this.SuspendLayout();
            // 
            // tvData
            // 
            this.tvData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tvData.Location = new System.Drawing.Point(12, 12);
            this.tvData.Name = "tvData";
            this.tvData.Size = new System.Drawing.Size(271, 285);
            this.tvData.TabIndex = 3;
            // 
            // TreeFromTable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(295, 309);
            this.Controls.Add(this.tvData);
            this.Name = "TreeFromTable";
            this.Text = "TreeFromTable";
            this.Load += new System.EventHandler(this.TreeFromTable_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TreeView tvData;
    }
}

