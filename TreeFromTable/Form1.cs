﻿using System;
using System.IO;
using System.Data;
using System.Windows.Forms;
using System.Linq;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace TreeFromTable
{
    #region Классы экранирующие таблицы

    [Table(Name = "catalog")]
    public class Catalog
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id { get; set; }

        [Column(Name = "name")]
        public string Name { get; set; }
    }

    [Table(Name = "aggregate")]
    public class Aggregate
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id { get; set; }

        [Column(Name = "name")]
        public string Name { get; set; }

        [Column(Name = "catalog_id")]
        public int CatalogId { get; set; }
    }

    [Table(Name = "model")]
    public class Model
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id { get; set; }

        [Column(Name = "name")]
        public string Name { get; set; }

        [Column(Name = "aggregate_id")]
        public int AggregateId { get; set; }
    }

    [Table(Name = "catalog_level")]
    public class CatalogLevel
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id { get; set; }

        [Column(Name = "parent_id")]
        public int? ParentId { get; set; }

        [Column(Name = "name")]
        public string Name { get; set; }
    }

    #endregion

    public partial class TreeFromTable : Form
    {
        public TreeFromTable()
        {
            InitializeComponent();
        }

        private void TreeFromTable_Load(object sender, EventArgs e)
        {
            DataContext db = new DataContext($@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={Directory.GetCurrentDirectory()}\MyDatabase.mdf;Integrated Security=True");
           
            //чистим таблицу catalog_level
            db.ExecuteCommand("TRUNCATE TABLE catalog_level");

            DataTransfer(db);
            TreeFilling(db.GetTable<CatalogLevel>().OrderBy(x => x.Id));
        }

        #region Метод переноса данных
        /// <summary>
        /// Метод переноса данных в таблицу catalog_level
        /// </summary>
        /// <param name="db">DataContext</param>
        private void DataTransfer(DataContext db)
        {
            #region Перенос данных из таблицы catalog
            foreach (var insertCatalog in db.GetTable<Catalog>().OrderBy(x => x.Id))
            {
                CatalogLevel cl = new CatalogLevel();
                cl.Name = insertCatalog.Name;
                cl.ParentId = null;
                db.GetTable<CatalogLevel>().InsertOnSubmit(cl);
                db.SubmitChanges();
            }
            #endregion

            #region Перенос данных из таблицы aggregate
            var insertAggregate = from aggregateTable in db.GetTable<Aggregate>()
                                  join catalogTable in db.GetTable<Catalog>() on aggregateTable.CatalogId equals catalogTable.Id
                                  join catalogLevelTable in db.GetTable<CatalogLevel>() on catalogTable.Name equals catalogLevelTable.Name
                                  orderby aggregateTable.Id
                                  select new
                                  {
                                      catalogLevelTable.Id,
                                      aggregateTable.Name
                                  };

            foreach (var insAggr in insertAggregate)
            {
                CatalogLevel cl = new CatalogLevel();
                cl.Name = insAggr.Name;
                cl.ParentId = insAggr.Id;
                db.GetTable<CatalogLevel>().InsertOnSubmit(cl);
                db.SubmitChanges();
            }
            #endregion

            #region Перенос данных из таблицы model
            var t1 = from modelTable in db.GetTable<Model>()
                     join aggregateTable in db.GetTable<Aggregate>() on modelTable.AggregateId equals aggregateTable.Id
                     join catalogTable in db.GetTable<Catalog>() on aggregateTable.CatalogId equals catalogTable.Id
                     select new
                     {
                         catName = catalogTable.Name,
                         aggrName = aggregateTable.Name,
                         modId = modelTable.Id,
                         modName = modelTable.Name
                     };

            var t2 = from catalogLevelTable1 in db.GetTable<CatalogLevel>()
                     join catalogLevelTable2 in db.GetTable<CatalogLevel>() on catalogLevelTable1.Id equals catalogLevelTable2.ParentId
                     select new
                     {
                         catName = catalogLevelTable1.Name,
                         id = catalogLevelTable2.Id,
                         aggrName = catalogLevelTable2.Name
                     };

            var insertModel = from tab1 in t1
                              from tab2 in t2
                              where tab1.aggrName == tab2.aggrName
                              where tab1.catName == tab2.catName
                              orderby tab1.modId
                              select new
                              {
                                  Id = tab2.id,
                                  Name = tab1.modName
                              };

            foreach (var insMod in insertModel)
            {
                CatalogLevel cl = new CatalogLevel();
                cl.Name = insMod.Name;
                cl.ParentId = insMod.Id;
                db.GetTable<CatalogLevel>().InsertOnSubmit(cl);
                db.SubmitChanges();
            }
            #endregion
        }
        #endregion

        #region Метод отрисовки TreeView
        /// <summary>
        /// Метод отрисовки TreeView
        /// </summary>
        /// <param name="data">CatalogLevel</param>
        private void TreeFilling(IQueryable<CatalogLevel> data)
        {
            foreach (var d in data)
            {
                if (!d.ParentId.HasValue)
                    tvData.Nodes.Add(d.Id.ToString(), d.Name);
                else
                    if (tvData.Nodes.Find(d.ParentId.ToString(), true).Length > 0)
                        tvData.Nodes.Find(d.ParentId.ToString(), true)[0].Nodes.Add(d.Id.ToString(), d.Name);
            }
            tvData.ExpandAll();
        }
        #endregion

    }
}
